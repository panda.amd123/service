from django.forms import model_to_dict
from django.http import Http404
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Client, Distribution, Message
from .serializers import ClientSerializer, MessageSerializer, DistributionSerializer


class ClientAPI(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageAPI(viewsets.ReadOnlyModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class DistributionAPI(viewsets.ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer

    @action(methods=['get'], detail=False)
    def general_statistics(self, request):
        total_count = Distribution.objects.count()
        distributions = Distribution.objects.values('pk')
        result = {}

        for distribution in distributions:
            statistic = {'Доставлено': 0, 'Не доставлено': 0}
            total_sent = Message.objects.filter(distribution_id=distribution['pk']).all().filter(sending_status='Доставлено').count()
            total_no_sent = Message.objects.filter(distribution_id=distribution['pk']).all().filter(sending_status='Не доставлено').count()
            statistic['Доставлено'] = total_sent
            statistic['Не доставлено'] = total_no_sent
            result[distribution['pk']] = statistic

        results = {'Общее количество рассылок': total_count,
                   'Результаты рассылок': ''}
        results['Результаты рассылок'] = result
        return Response(results)

    @action(methods=['get'], detail=True)
    def detailed_statistics(self, request, pk):
        try:
            distribution = Distribution.objects.get(pk=pk)
        except Distribution.DoesNotExist:
            raise Http404
        detail_message = Message.objects.filter(distribution_id=pk).all()
        return Response(MessageSerializer(detail_message, many=True).data)
