import os


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'service.settings')
import django

django.setup()

from django.utils.timezone import now
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import *


class TestStat(APITestCase):

    def test_distribution(self):
        distribution_count = Distribution.objects.all().count()
        distribution_create = {
            'date_start': now(),
            'date_end': now(),
            'time_start': now().time(),
            'time_end': now().time(),
            'text': "Тест",
            'tag': "test",
            'mobile_operator_code': '999'
        }
        response = self.client.post('http://127.0.0.1:8000/api/distribution/', distribution_create)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['text'], 'Тест')
        self.assertIsInstance(response.data['text'], str)

    def test_client(self):
        client_count = Client.objects.all().count()
        client_create = {
            "phone_number": '79999999999',
            "tag": "test", "timezone": "UTC"
        }
        response = self.client.post('http://127.0.0.1:8000/api/client/', client_create)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['phone_number'], '79999999999')
        self.assertIsInstance(response.data['phone_number'], str)

    def test_message(self):
        response = self.client.get('http://127.0.0.1:8000/api/message/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
