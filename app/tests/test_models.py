import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'service.settings')
import django
django.setup()

from django.utils.timezone import now
from rest_framework.test import APITestCase

from ..models import *


class TestModel(APITestCase):

    def test_creates_clients(self):
        client = Client.objects.create(phone_number='79999999999', mobile_operator_code='999', tag='Eysk', timezone='UTC')
        self.assertIsInstance(client, Client)
        self.assertEqual(client.phone_number, '79999999999')

    def test_creates_messages(self):
        self.test_creates_distributions()
        self.test_creates_clients()
        message = Message.objects.create(sending_status='Доставлен', distribution_id=1, client_id=1)
        self.assertIsInstance(message, Message)
        self.assertEqual(message.sending_status, 'Доставлен')

    def test_creates_distributions(self):
        distribution = Distribution.objects.create(date_start=now(), date_end=now(), time_start=now().time(), time_end=now().time(), text='Simple text', tag='Eysk')
        self.assertIsInstance(distribution, Distribution)
        self.assertEqual(distribution.tag, 'Eysk')
