from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

from .models import *
from .tasks import send_message


@receiver(post_save, sender=Distribution)
def create_message(sender, instance, created, **kwargs):
    if created:
        distribution = Distribution.objects.filter(id=instance.id).first()
        clients = Client.objects.filter(Q(mobile_operator_code=distribution.mobile_operator_code) |
                                        Q(tag=distribution.tag)).all()

        for client in clients:
            Message.objects.create(
                sending_status='Не доставлено',
                client_id=client.id,
                distribution_id=instance.id,
            )
            message = Message.objects.filter(distribution_id=instance.id, client_id=client.id).first()
            data = {
                'id': message.id,
                'phone': client.phone_number,
                'text': distribution.text,
            }
            client_id = client.id
            distribution_id = distribution.id

            if instance.to_send:
                send_message.apply_async(
                    (data, client_id, distribution_id),
                    expires=distribution.date_end,
                )
            else:
                send_message.apply_async(
                    (data, client_id, distribution_id),
                    eta=distribution.date_start,
                    expires=distribution.date_end,
                )
