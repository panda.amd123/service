import os
import requests
import pytz
import datetime
from dotenv import load_dotenv
from celery.utils.log import get_task_logger

from .models import *
from service.celery import app

logger = get_task_logger(__name__)

load_dotenv()
URL = os.getenv("URL")
TOKEN = os.getenv("TOKEN")


@app.task(bind=True, retry_backoff=True)
def send_message(self, data, client_id, distribution_id, url=URL, token=TOKEN):
    mail = Distribution.objects.get(id=distribution_id)
    client = Client.objects.get(id=client_id)
    timezone = pytz.timezone(client.timezone)
    now = datetime.datetime.now(timezone)

    if mail.time_start <= now.time() <= mail.time_end:
        header = {
            'Authorization': f'Bearer {token}',
            'Content-Type': 'application/json',
        }
        try:
            requests.post(url=url + str(data['id']), headers=header, json=data)
        except requests.exceptions.RequestException as exc:
            logger.error(f"Сообщение: {data['id']} ошибка")
            raise self.retry(exc=exc)
        else:
            logger.info(f"Сообщение: {data['id']}, Статус: 'Доставлен'")
            Message.objects.filter(id=data['id']).update(sending_status='Доставлен')
    else:
        time = 24 - (int(now.time().strftime('%H:%M:%S')[:2]) - int(mail.time_start.strftime('%H:%M:%S')[:2]))
        logger.info(
            f"Сообщение: {data['id']}, "
            f"Повторить через: {60 * 60 * time} сек"
        )
        return self.retry(countdown=60 * 60 * time)
