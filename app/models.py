import pytz
from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator


class Distribution(models.Model):
    date_start = models.DateTimeField(verbose_name='Дата начала')
    date_end = models.DateTimeField(verbose_name='Дата окончания')
    time_start = models.TimeField(verbose_name='Время начала')
    time_end = models.TimeField(verbose_name='Время окончания')
    text = models.TextField(verbose_name='Текст', max_length=255)
    tag = models.CharField(verbose_name='Метка', max_length=100, blank=True)
    mobile_operator_code = models.CharField(verbose_name='Код оператора', max_length=3, blank=True)

    @property
    def to_send(self):
        if self.date_start <= timezone.now() <= self.date_end:
            return True
        else:
            return False

    def __str__(self):
        return f'{self.tag}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(verbose_name='Телефон', validators=[
        RegexValidator(
            regex=r'^7\d{10}$',
            message='Формат номера телефона: 7xxxxxxxxxx (x - цифра от 0 до 9)',
        )
    ], unique=True, max_length=11)
    mobile_operator_code = models.CharField(verbose_name='Код оператора', max_length=3, editable=False)
    tag = models.CharField(verbose_name='Метка', max_length=100, blank=True)
    timezone = models.CharField(verbose_name='Часовой пояс', max_length=32, choices=TIMEZONES, default='UTC')

    def save(self, *args, **kwargs):
        self.operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.phone_number}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    SENT = "Доставлено"
    NO_SENT = "Не доставлено"

    STATUS_CHOICES = [
        (SENT, "Доставлено"),
        (NO_SENT, "Не доставлено"),
    ]

    time_create = models.DateTimeField(verbose_name='Время создания', auto_now_add=True)
    sending_status = models.CharField(verbose_name='Статус', max_length=15, choices=STATUS_CHOICES)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE, related_name='distribution')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='distribution')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'{self.client}'
